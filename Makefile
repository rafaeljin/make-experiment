# 'all' is the first target
all:
	@echo "Makefile 'all'"

target:
	touch target
target2:
	touch target2

# replaces the depedencies of 'all'
include Makefrag

clean:
	rm -f target*

.PHONY: all clean variable func

# two types of vars
var1 := a
var2 = $(var1) # try with ':='
var1 := b
# var1 = $(var2)
variable2:;@echo $(var2)
variable1:;@echo $(var1)
variable: variable1 variable2

# function
var3 := "abcde"
var4 := $(subst bcd,234,$(var3))
func:;@echo $(var4)

# wildcard
SOURCES := $(wildcard *.c)
OBJECTS := $(patsubst %.c,%.o, $(SOURCES))
wildcard-debug:
	@echo $(OBJECTS)
all: $(OBJECTS)
%.o: %.c
	cp $< $@
